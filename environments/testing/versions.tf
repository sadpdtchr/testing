terraform {
  required_providers {
    rabbitmq = {
      source = "terraform-providers/rabbitmq"
    }
  }
  required_version = ">= 0.13"
}
