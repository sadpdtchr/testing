resource "rabbitmq_exchange" "test" {
  name  = "test"
  vhost = "${rabbitmq_vhost.test.name}"

  settings {
    type        = "fanout"
    durable     = false
    auto_delete = true
  }
}